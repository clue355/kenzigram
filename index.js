import express from "express";
import ejs from "ejs";
import path from "path";
import fs from "fs";
import multer from "multer";
import { v4 as uuidv4 } from "uuid";

const app = express();

const __dirname = path.resolve(path.dirname(""));
const staticDirectory = path.resolve(__dirname, "public");

//******************** referenced https://youtu.be/9Qzmri1WaaE image uploading with multer video ***

const storage = multer.diskStorage({
  destination: "./public/uploads/",
  filename: function (req, file, cb) {
    cb(null, file.fieldname + "-" + Date.now() + path.extname(file.originalname));
  },
});

const upload = multer({
  storage: storage,
  limits: { fileSize: 1000000000 },
  fileFilter: function (req, file, cb) {
    checkFileType(file, cb);
  },
}).single("pic");

function checkFileType(file, cb) {
  const filetypes = /jpeg|jpg|png|gif/;
  const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
  const mimetype = filetypes.test(file.mimetype);
  if (mimetype && extname) {
    return cb(null, true);
  } else {
    cb("Error Images Only");
  }
}

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(staticDirectory));

// app.set("view engine", "ejs");

app.get("/", (req, res) => {
  fs.readdir("./public/uploads/", (err, items) => {
    console.log(items);
    let images = items.map((photo) => `<img src="/uploads/${photo}" alt="photo"> `);
    res.send(` 
  <head>

    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css" />
    <title>Document</title>
  </head>
<h1>Welcome to Kenziegram!</h1>
<div>
  <h1>File Upload</h1>
  <form action="/upload" method="POST" enctype="multipart/form-data">
    <div class="file-field input-field">
      <div class="btn">
        <span>File</span>
        <input name="pic" type="file" multiple />
      </div>
      <div class="file-path-wrapper">
        <input class="file-path validate" type="text" placeholder="Upload one or more files" />
      </div>
    </div>
    <button type="submit" class="btn">Submit</button>
  </form>
  <br />
</div>
${images}
`);
  });
});

app.post("/upload", (req, res) => {
  upload(req, res, (err) => {
    if (err) {
      res.render("index", {
        msg: err,
      });
    } else {
      if (req.file == undefined) {
        res.render("index", {
          msg: "Error no file selected",
        });
      } else {
        res.send(`
        <div><img src="/uploads/${req.file.filename}" >
        <a href="/">
        <button>Home</button>
        </a></div>
        `);
      }
    }
  });
});

const port = 3000;
app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`));

process.on("unhandledRejection", (reason, p) => {
  console.error("unhandled Rejection at :", p, "reason:", reason);

  process.exit(0);
});
